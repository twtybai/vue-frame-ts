/**
 * Created by henian.xu on 2019/3/22.
 *
 */

export { default as XForm } from './Form.vue';
export { default as FormItem } from './FormItem.vue';
