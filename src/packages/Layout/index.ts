/**
 * Created by henian.xu on 2019/3/21.
 *
 */

export { default as Page } from './Page.vue';
export { default as Container } from './Container.vue';
