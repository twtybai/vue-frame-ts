<!-- Created by henian.xu on 2019/3/26. -->

<template>
    <div
        v-if="tiling"
        class="x-select tiling"
    >
        <div
            :class="['item',{active:isSelected(item.id)}]"
            v-for="(item,index) in data"
            :key="item.id || index"
            @click="onItem(item)"
        >
            {{ item.name }}
            <!--<sapn
                class="extra"
                slot="extra"
                v-show="isSelected(item.id)"
            ><i class="f-icon tc-green">&#xf017;</i></sapn>-->
        </div>
    </div>
    <div
        v-else
        class="x-select"
    >
        <div
            class="out-show"
            @click="open()"
        >
            {{ outShow || placeholder }}
        </div>

        <!-- popup -->
        <transition
            v-if="!lazy"
            name="show"
        >
            <div
                ref="popup"
                v-show="popupShow"
                class="popup popup-select"
                @click.self="close"
            >
                <transition name="popup-down">
                    <div
                        class="inner"
                        v-show="popupShow"
                    >
                        <div
                            v-if="label || subLabel"
                            class="header"
                        >
                            <XLabel
                                :label="label"
                                :sub-label="subLabel"
                            />
                        </div>
                        <div class="body">
                            <CellGroup border>
                                <Cell
                                    v-for="(item) in options"
                                    :key="item.id"
                                    :label="item.name"
                                    @click="onItem(item)"
                                >
                                    <template v-slot:extra>
                                        <div class="extra">
                                            <XIcon
                                                v-if="isSelected(item.id)"
                                                content="f017"
                                                theme="success"
                                            />
                                        </div>
                                    </template>
                                </Cell>
                            </CellGroup>
                        </div>
                        <!--<div class="footer">
                            <XButton label="label" />
                            <XButton label="label" />
                        </div>-->
                    </div>
                </transition>
            </div>
        </transition>
    </div>
</template>

<script lang='ts'>
import { Component, Prop, Mixins } from 'vue-property-decorator';
import labelMixin from '../mixins/labelMixin';
import popupMixin from '../mixins/popupMixin.vue';

interface optionType {
    id: string | number;
    name: string;
    original: object;
}

@Component
export default class XSelect extends Mixins(labelMixin, popupMixin) {
    protected lazy = true;

    @Prop({ type: [Number, String, Array] })
    readonly value!: number | string | number[] | string[];
    @Prop({ type: Array, default: () => [] })
    readonly data!: object[];
    @Prop(Boolean)
    readonly multiple!: boolean;
    @Prop(Boolean)
    readonly bitwise!: boolean;
    @Prop(Boolean)
    readonly tiling!: boolean;
    @Prop({ type: Object, default: () => ({ id: 'id', name: 'name' }) })
    readonly prop!: { id: string; name: string };

    private get model() {
        return this.value;
    }
    private set model(val) {
        const { multiple, bitwise, value } = this;
        if (!multiple) {
            this.$emit('input', val);
        } else if (!bitwise && Array.isArray(value)) {
            const value_: any = [...value];
            const index = value_.indexOf(val);
            if (index === -1) {
                value_.push(val);
            } else {
                value_.splice(index, 1);
            }
            this.$emit('input', value_);
        } else {
            if (+value & +val) {
                this.$emit('input', +value ^ +val);
            } else {
                this.$emit('input', +value | +val);
            }
        }
    }
    private get options() {
        const { data, prop } = this;
        return data.reduce((pre: optionType[], curr: any) => {
            pre.push({
                id: curr[prop.id],
                name: curr[prop.name],
                original: curr,
            } as optionType);
            return pre;
        }, []);
    }
    private get dataMap(): { [key: string]: optionType } {
        return this.options.reduce((p: any, c: optionType) => ((p[c.id] = c), p), {});
    }
    private get outShow() {
        const { multiple, bitwise, model, dataMap, data } = this;
        if (!data.length) {
            return '';
        } else if (!multiple && (typeof model === 'number' || typeof model === 'string')) {
            return (dataMap[model] || ({} as optionType)).name;
        } else if (!bitwise && Array.isArray(model)) {
            return (model as any[]).reduce((p, c) => (p.push(dataMap[c].name), p), []).join(',');
        } else {
            // TODO 这个算法太慢了应该优化
            return Object.values(dataMap)
                .reduce((p: string[], c) => {
                    if (!(+model & (c.id as number))) return p;
                    p.push(dataMap[c.id].name);
                    return p;
                }, [])
                .join(',');
        }
    }

    private isSelected(id: number | string) {
        const { multiple, bitwise, model } = this;
        if (!multiple) {
            return model + '' === id + '';
        } else if (!bitwise && Array.isArray(model)) {
            return (model as any[]).some(item => item + '' === id + '');
        } else {
            return +model & (id as number);
        }
    }

    private onItem(item: optionType) {
        this.model = item.id;
        if (!this.multiple) this.close();
    }
    private onClose() {
        this.close();
    }
}
</script>

<style lang="scss">
.x-select {
    flex: 1 1 1%;
    > .out-show {
        line-height: $formItemHeight;
    }

    &.tiling {
        display: flex;
        flex-direction: row;
        justify-content: flex-start;
        align-items: center;
        min-height: $formItemHeight;
        > .item {
            border: 1px solid $color-border;
            border-radius: $padding-small;
            padding: 0 $padding-small;
            + .item {
                margin-left: $margin-small;
            }

            &.active {
                color: $color-success;
                border-color: $color-success;
                /*&:after {
                    content: '\f017';
                    @include make-icon();
                }*/
            }
        }
    }
}
.popup.popup-select {
    justify-content: flex-end;
    align-items: stretch;
    > .inner {
        > .header {
            border-bottom: 1px solid $color-border;
        }
        > .body {
            padding: 0;
        }
    }
}
</style>
