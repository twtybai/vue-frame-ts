import { Component, Prop, Mixins } from 'vue-property-decorator';
import commonMixin from './commonMixin';

@Component
export default class labelMixin extends Mixins(commonMixin) {
    @Prop([String, Number])
    readonly label?: string | number;
    @Prop([String, Number])
    readonly subLabel?: string | number;

    // TODO 返回的 any
    public get __labelProps(): any {
        const { theme, label, subLabel, __labelScopedSlots: scopedSlots } = this;
        return { theme, label, subLabel, scopedSlots };
    }
    protected get __labelScopedSlots() {
        // return { ...this.$scopedSlots }; // 让标签直接应用当前组件的插槽中的 label;subLabel;default
        return {};
    }
}
