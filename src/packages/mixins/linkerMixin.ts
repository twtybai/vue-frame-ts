/**
 * Created by henian.xu on 2019/3/21.
 * 超链接/路由混合器
 */

const urlReg = new RegExp(/[a-zA-z]+:\/\/[^\s]*/);

import { Vue, Component, Prop } from 'vue-property-decorator';
import { Route } from 'vue-router';

@Component
export default class LinkerMixin extends Vue {
    @Prop([String, Object])
    readonly to?: string | Route;
    @Prop({ type: String, default: 'a' })
    readonly tag!: string;
    @Prop(Boolean)
    readonly exact?: boolean;
    @Prop(Boolean)
    readonly append?: boolean;
    @Prop(Boolean)
    readonly replace?: boolean;
    @Prop(String)
    readonly activeClass?: string;
    @Prop(String)
    readonly exactActiveClass?: string;
    @Prop({ type: [String, Object], default: 'click' })
    readonly event!: string | object;
    @Prop(Boolean)
    readonly disabled?: boolean;

    protected get isUrl() {
        const { to } = this;
        if (!to) return false;
        else if (typeof to === 'string') return urlReg.test(to);
        else if (to.path) return urlReg.test(to.path);
        return false;
    }
    protected get __linkerProps() {
        const { to, tag, exact, append, replace, activeClass, exactActiveClass, event, disabled } = this;
        if (!to || disabled) {
            return {};
        } else if (this.isUrl) {
            return {
                is: 'a',
                href: to,
            };
        } else {
            return {
                is: 'router-link',
                to,
                tag,
                exact,
                append,
                replace,
                activeClass,
                exactActiveClass,
                event,
            };
        }
    }
}
