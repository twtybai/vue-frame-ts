/**
 * Created by henian.xu on 2019/3/20.
 *
 */

import { Vue, Component, Prop } from 'vue-property-decorator';

@Component
export default class commonMixin extends Vue {
    @Prop(String)
    readonly theme?: string;
    @Prop(String)
    readonly size?: string;
    @Prop(Boolean)
    readonly disabled?: boolean;
    @Prop(Boolean)
    readonly readonly?: boolean;

    protected get pointer() {
        // TODO 这里的 this as any 感觉有点不有应该找找看typescript有没有其它的解决方法
        const { to, $listeners, link } = this as any;
        return link || !!to || !!$listeners.click;
    }
    protected get __iconProps() {
        const { theme, iconTheme, subLabel, icon, iconSize, disabled } = this as any;
        return { content: icon, theme: iconTheme || theme, size: iconSize || (subLabel ? 'big' : ''), disabled };
    }
}
