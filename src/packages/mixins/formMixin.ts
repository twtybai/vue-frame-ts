/**
 * Created by henian.xu on 2019/3/25.
 *
 */

import { Vue, Component, Prop } from 'vue-property-decorator';
import { getUniqueId } from '../utils/Common';

@Component
export default class FormMixin extends Vue {
    @Prop({
        type: String,
        default: () => getUniqueId(`field-`),
    })
    readonly id!: string;
    @Prop({ type: String, default: '请输入...' })
    readonly placeholder!: string;
    @Prop(Boolean)
    readonly readonly?: boolean;
    @Prop(Boolean)
    readonly disabled?: boolean;
    @Prop(Boolean)
    readonly isClear?: boolean;

    private get __attrProps() {
        const { id, placeholder, readonly, disabled, $attrs } = this;
        return { ...$attrs, id, placeholder, readonly, disabled };
    }
}
