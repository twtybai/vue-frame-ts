/**
 * Created by henian.xu on 2019/3/21.
 *
 */

export { default as Tab } from './Tab.vue';
export { default as TabNav } from './TabNav.vue';
export { default as TabPanel } from './TabPanel.vue';
