/**
 * Created by henian.xu on 2019/3/26.
 *
 */

export function getPopupWrap(target: Element): Element {
    const childNodes = target.childNodes;
    let popup;
    const lng = childNodes.length - 1;
    for (let i = lng; i >= 0; i--) {
        const node: Element = childNodes[i] as Element;
        if (node.nodeType !== 1) continue;
        if (/\bpopup-wrap\b/.test(node.className)) {
            popup = node;
            break;
        }
    }
    if (!popup) {
        popup = document.createElement('div');
        popup.classList.add('popup-wrap');
        target.appendChild(popup);
    }
    return popup;
}
