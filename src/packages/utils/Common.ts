/**
 * Created by henian.xu on 2019/3/22.
 *
 */

import any = jasmine.any;

/**
 * 获取对象类型
 * @param obj
 * @returns {string}
 */
export function getType<T>(obj: T): string {
    return Object.prototype.toString.call(obj).slice(8, -1);
}

let autoIncrement = 0;

/**
 * 创建不重复的 Id
 * @param prefix
 * @returns {string}
 */
export function getUniqueId(prefix = '') {
    autoIncrement++;
    const cDate = new Date().getTime();
    const offDate = new Date(2010, 1, 1).getTime();
    const offset = cDate - offDate;
    return prefix + parseFloat(offset + '').toString(16) + autoIncrement;
}

/**
 * 提取对象深层值
 * @param obj
 * @param path
 * @param strict
 * @returns {string}
 */
export function pluckDeep(obj: object, path: string | string[], strict = false): any {
    let pathList: string[];
    if (Array.isArray(path)) {
        pathList = path;
    } else {
        pathList = path.split('.');
    }
    const lastIndex = pathList.length - 1;
    return pathList.reduce((prev: any, curr: string, index) => {
        if (curr in prev) {
            return prev[curr];
        } else if (strict) {
            throw new Error(`[pluckDeep]:${path}超出对像范围`);
        } else if (index !== lastIndex) {
            return {};
        }
    }, obj);
}

/**
 * 防抖动
 * @param fun
 * @param delay
 * @param firstRun
 * @returns {function(*): Promise<any>}
 */
export function debounce(fun: Function, delay = 200, firstRun = false): (...args: any) => Promise<any> {
    let timeId: number | null;
    return function(this: any, ...args: any) {
        const self: any = this;
        const _args = args;
        return new Promise(resolve => {
            if (firstRun && timeId === null) {
                timeId = 0;
                resolve(fun.call(self, _args));
            } else {
                // 每次事件被触发，都会清除当前的timeer,然后重写设置超时调用
                clearTimeout(<number>timeId);
                timeId = setTimeout(() => {
                    timeId = null;
                    resolve(fun.call(self, _args));
                }, delay);
            }
        });
    };
}
/**
 * Check whether an object has the property.
 */
var hasOwnProperty = Object.prototype.hasOwnProperty;
function hasOwn(obj: any, key: any) {
    return hasOwnProperty.call(obj, key);
}
/**
 * 是否 VNode
 * @param node
 * @returns {boolean|boolean|*}
 */
export function isVNode(node: any) {
    return node !== null && typeof node === 'object' && hasOwn(node, 'componentOptions');
}
