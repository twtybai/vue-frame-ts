/**
 * Created by henian.xu on 2018/5/30.
 *
 */

const noop: (value: number) => void = () => {};

export default class Animate {
    // this.dom = dom; // 进行运动的dom 节点
    private startTime = 0; // 动画开始时间
    private currPos = 0;
    private startPos = 0; // 动画开始时，dom 节点的位置，即dom 的初始位置
    private endPos = 0; // 动画结束时，dom 节点的位置，即dom 的目标位置
    private easing: (t: number, b: number, c: number, d: number, s?: number) => number = Animate.easeOutQuart; // 缓动算法
    private duration = 0; // 动画持续时间
    private timeId = 0; // 动画时间id

    private stepCallback: Function;
    private completedCallback: Function;
    private stopCallback: Function;
    constructor(stepCallback = noop, completedCallback = noop, stopCallback = noop) {
        this.stepCallback = stepCallback;
        this.completedCallback = completedCallback;
        this.stopCallback = stopCallback;
    }

    public stop() {
        clearInterval(this.timeId);
        this.stopCallback(this.currPos);
    }

    start(
        startPos: number,
        endPos: number,
        duration: number,
        easing: (t: number, b: number, c: number, d: number, s?: number) => number = Animate.easeOutQuart,
    ) {
        this.startTime = +new Date(); // 动画启动时间
        this.startPos = startPos; // this.dom.getBoundingClientRect()[propertyName]; // dom 节点初始位置
        this.endPos = endPos; // dom 节点目标位置
        this.duration = duration; // 动画持续事件
        this.easing = easing; // 缓动算法
        this.timeId = setInterval(() => {
            if (this.step() === false) {
                clearInterval(this.timeId);
            }
        }, 10);
    }

    step() {
        const currTime = +new Date();
        if (currTime >= this.startTime + this.duration) {
            // (1)
            this.currPos = this.endPos;
            this.completedCallback(this.endPos); // 修正位置
            return false;
        }
        this.currPos = this.easing(
            currTime - this.startTime,
            this.startPos,
            this.endPos - this.startPos,
            this.duration,
        );
        return this.stepCallback(this.currPos);
    }

    // 缓动方法

    static easeInQuad(t: number, b: number, c: number, d: number) {
        return c * (t /= d) * t + b;
    }

    static easeOutQuad(t: number, b: number, c: number, d: number) {
        return -c * (t /= d) * (t - 2) + b;
    }

    static easeInOutQuad(t: number, b: number, c: number, d: number) {
        if ((t /= d / 2) < 1) return (c / 2) * t * t + b;
        return (-c / 2) * (--t * (t - 2) - 1) + b;
    }

    static easeInCubic(t: number, b: number, c: number, d: number) {
        return c * (t /= d) * t * t + b;
    }

    static easeOutCubic(t: number, b: number, c: number, d: number) {
        return c * ((t = t / d - 1) * t * t + 1) + b;
    }

    static easeInOutCubic(t: number, b: number, c: number, d: number) {
        if ((t /= d / 2) < 1) return (c / 2) * t * t * t + b;
        return (c / 2) * ((t -= 2) * t * t + 2) + b;
    }

    static easeInQuart(t: number, b: number, c: number, d: number) {
        return c * (t /= d) * t * t * t + b;
    }

    static easeOutQuart(t: number, b: number, c: number, d: number) {
        return -c * ((t = t / d - 1) * t * t * t - 1) + b;
    }

    static easeInOutQuart(t: number, b: number, c: number, d: number) {
        if ((t /= d / 2) < 1) return (c / 2) * t * t * t * t + b;
        return (-c / 2) * ((t -= 2) * t * t * t - 2) + b;
    }

    static easeInQuint(t: number, b: number, c: number, d: number) {
        return c * (t /= d) * t * t * t * t + b;
    }

    static easeOutQuint(t: number, b: number, c: number, d: number) {
        return c * ((t = t / d - 1) * t * t * t * t + 1) + b;
    }

    static easeInOutQuint(t: number, b: number, c: number, d: number) {
        if ((t /= d / 2) < 1) return (c / 2) * t * t * t * t * t + b;
        return (c / 2) * ((t -= 2) * t * t * t * t + 2) + b;
    }

    static easeInSine(t: number, b: number, c: number, d: number) {
        return -c * Math.cos((t / d) * (Math.PI / 2)) + c + b;
    }

    static easeOutSine(t: number, b: number, c: number, d: number) {
        return c * Math.sin((t / d) * (Math.PI / 2)) + b;
    }

    static easeInOutSine(t: number, b: number, c: number, d: number) {
        return (-c / 2) * (Math.cos((Math.PI * t) / d) - 1) + b;
    }

    static easeInExpo(t: number, b: number, c: number, d: number) {
        return t === 0 ? b : c * Math.pow(2, 10 * (t / d - 1)) + b;
    }

    static easeOutExpo(t: number, b: number, c: number, d: number) {
        return t === d ? b + c : c * (-Math.pow(2, (-10 * t) / d) + 1) + b;
    }

    static easeInOutExpo(t: number, b: number, c: number, d: number) {
        if (t === 0) return b;
        if (t === d) return b + c;
        if ((t /= d / 2) < 1) return (c / 2) * Math.pow(2, 10 * (t - 1)) + b;
        return (c / 2) * (-Math.pow(2, -10 * --t) + 2) + b;
    }

    static easeInCirc(t: number, b: number, c: number, d: number) {
        return -c * (Math.sqrt(1 - (t /= d) * t) - 1) + b;
    }

    static easeOutCirc(t: number, b: number, c: number, d: number) {
        return c * Math.sqrt(1 - (t = t / d - 1) * t) + b;
    }

    static easeInOutCirc(t: number, b: number, c: number, d: number) {
        if ((t /= d / 2) < 1) return (-c / 2) * (Math.sqrt(1 - t * t) - 1) + b;
        return (c / 2) * (Math.sqrt(1 - (t -= 2) * t) + 1) + b;
    }

    static easeInElastic(t: number, b: number, c: number, d: number) {
        let s = 1.70158;
        let p = 0;
        let a = c;
        if (t === 0) return b;
        if ((t /= d) === 1) return b + c;
        if (!p) p = d * 0.3;
        if (a < Math.abs(c)) {
            a = c;
            s = p / 4;
        } else {
            s = (p / (2 * Math.PI)) * Math.asin(c / a);
        }
        return -(a * Math.pow(2, 10 * (t -= 1)) * Math.sin(((t * d - s) * (2 * Math.PI)) / p)) + b;
    }

    static easeOutElastic(t: number, b: number, c: number, d: number) {
        let s = 1.70158;
        let p = 0;
        let a = c;
        if (t === 0) return b;
        if ((t /= d) === 1) return b + c;
        if (!p) p = d * 0.3;
        if (a < Math.abs(c)) {
            a = c;
            s = p / 4;
        } else {
            s = (p / (2 * Math.PI)) * Math.asin(c / a);
        }
        return a * Math.pow(2, -10 * t) * Math.sin(((t * d - s) * (2 * Math.PI)) / p) + c + b;
    }

    static easeInOutElastic(t: number, b: number, c: number, d: number) {
        let s = 1.70158;
        let p = 0;
        let a = c;
        if (t === 0) return b;
        if ((t /= d / 2) === 2) return b + c;
        if (!p) p = d * (0.3 * 1.5);
        if (a < Math.abs(c)) {
            a = c;
            s = p / 4;
        } else {
            s = (p / (2 * Math.PI)) * Math.asin(c / a);
        }
        if (t < 1) return -0.5 * (a * Math.pow(2, 10 * (t -= 1)) * Math.sin(((t * d - s) * (2 * Math.PI)) / p)) + b;
        return a * Math.pow(2, -10 * (t -= 1)) * Math.sin(((t * d - s) * (2 * Math.PI)) / p) * 0.5 + c + b;
    }

    static easeInBack(t: number, b: number, c: number, d: number, s?: number) {
        if (s === undefined) s = 1.70158;
        return c * (t /= d) * t * ((s + 1) * t - s) + b;
    }

    static easeOutBack(t: number, b: number, c: number, d: number, s?: number) {
        if (s === undefined) s = 1.70158;
        return c * ((t = t / d - 1) * t * ((s + 1) * t + s) + 1) + b;
    }

    static easeInOutBack(t: number, b: number, c: number, d: number, s?: number) {
        if (s === undefined) s = 1.70158;
        if ((t /= d / 2) < 1) return (c / 2) * (t * t * (((s *= 1.525) + 1) * t - s)) + b;
        return (c / 2) * ((t -= 2) * t * (((s *= 1.525) + 1) * t + s) + 2) + b;
    }

    /* easeInBounce(t: number, b: number, c: number, d: number) {
        return c - jQuery.easing.easeOutBounce(d - t, 0, c, d) + b;
    } */

    static easeOutBounce(t: number, b: number, c: number, d: number) {
        if ((t /= d) < 1 / 2.75) {
            return c * (7.5625 * t * t) + b;
        } else if (t < 2 / 2.75) {
            return c * (7.5625 * (t -= 1.5 / 2.75) * t + 0.75) + b;
        } else if (t < 2.5 / 2.75) {
            return c * (7.5625 * (t -= 2.25 / 2.75) * t + 0.9375) + b;
        } else {
            return c * (7.5625 * (t -= 2.625 / 2.75) * t + 0.984375) + b;
        }
    }

    /* easeInOutBounce(t: number, b: number, c: number, d: number) {
        if (t < d / 2) return jQuery.easing.easeInBounce(t * 2, 0, c, d) * 0.5 + b;
        return jQuery.easing.easeOutBounce(t * 2 - d, 0, c, d) * 0.5 + c * 0.5 + b;
    } */
}
