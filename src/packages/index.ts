/**
 * Created by henian.xu on 2019/2/27.
 *
 */
import _Vue, { VueConstructor } from 'vue';

const requireComponent = require.context('./', true, /index\.(vue|js|ts)$/);

function registerComponent<VC extends VueConstructor>(Vue: typeof _Vue, component: VC) {
    component.name ? Vue.component(component.name, component) : '';
}

export default {
    install(Vue: typeof _Vue) {
        requireComponent.keys().forEach((filePath: string) => {
            if (filePath.split('/').length !== 3) return;
            const components = requireComponent(filePath);
            // console.log(filePath, components);
            Object.keys(components).forEach((key: string) => {
                const component = components[key];
                registerComponent(Vue, component);
            });
        });
    },
};
