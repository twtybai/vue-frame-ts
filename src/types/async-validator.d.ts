/**
 * Created by henian.xu on 2019/3/26.
 *
 */

declare module 'async-validator' {
    export interface validRuleObj {
        trigger?: string;
    }
    export default class ValidatorSchema {
        constructor(descriptor: object);
        validate(source: object, options: object, callback: Function): Promise<any>;
    }
}
