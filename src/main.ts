import './entry';
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import packages from './packages';
import 'assets/style/app.scss';
// import * as utils from './utils';

// console.log(utils.Device);
// alert(JSON.stringify(utils.Device));

Vue.config.productionTip = false;

Vue.use(packages);

new Vue({
    router,
    store,
    render: h => h(App),
}).$mount('#app');
