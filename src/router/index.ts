import Vue from 'vue';
import Router from 'vue-router';

Vue.use(Router);

export default new Router({
    routes: [
        {
            path: '/',
            name: 'index',
            component: () => import(/* webpackChunkName: "main" */ 'pages/Index.vue'),
        },
        {
            path: '/home',
            name: 'home',
            component: () => import(/* webpackChunkName: "main" */ 'pages/Home.vue'),
        },
        {
            path: '/about',
            name: 'about',
            // route level code-splitting
            // this generates a separate chunk (about.[hash].js) for this route
            // which is lazy-loaded when the route is visited.
            component: () => import(/* webpackChunkName: "main" */ 'pages/About.vue'),
        },
    ],
});
