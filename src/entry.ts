/**
 * Created by henian.xu on 2019/3/20.
 *
 */

import GlobalVar from 'globalVar';

if (GlobalVar.vConsole) {
    import(/* webpackChunkName: "vconsole" */ 'vconsole').then(({ default: vConsole }) => {
        new vConsole({
            defaultPlugins: ['system', 'network', 'element', 'storage'], // 可以在此设定要默认加载的面板
            maxLogNumber: 1000,
            // disableLogScrolling: true,
            onReady() {
                // console.log('vConsole is ready.');
            },
            onClearLog() {
                // console.log('on clearLog');
            },
        });
    });
}
