/**
 * Created by henian.xu on 2019/2/26.
 *
 */

'use strict';
const path = require('path');
const webpack = require('webpack');

function resolve(dir) {
    return path.join(__dirname, '.', dir);
}

module.exports = {
    resolve: {
        alias: {
            '@': resolve('src'),
            pages: resolve('src/pages'),
            views: resolve('src/views'),
            api: resolve('src/api'),
            assets: resolve('src/assets'),
            packages: resolve('src/packages'),
            components: resolve('src/components'),
            config: resolve('src/config'),
            router: resolve('src/router'),
            store: resolve('src/store'),
            utils: resolve('src/utils'),
        },
    },
    plugins: [
        new webpack.ContextReplacementPlugin(
            // 需要被处理的文件目录位置
            /moment[/\\]locale/,
            // 正则匹配需要被包括进来的文件
            /(en|zh-cn)\.js/,
        ),
    ],
    externals: {
        globalVar: 'window.globalVar',
    },
};
