/**
 * Created by henian.xu on 2019/2/26.
 * vue 构建配置文件
 */

const webpackConfig = require('./webpack.config');

module.exports = {
    devServer: {
        port: 7070,
    },

    css: {
        sourceMap: true,
        loaderOptions: {
            sass: {
                data: `@import "assets/style/_index.scss";`,
            },
        },
    },

    // transpileDependencies: [/\bvue-echarts\b/, /\bresize-detector\b/],

    configureWebpack: webpackConfig,

    // outputDir: 'dist', // 输出
    // indexPath: 'index.html', //相对于 outputDir
    assetsDir: 'static', //相对于 outputDir
    // publicPath: '', //相对于 outputDir
};
